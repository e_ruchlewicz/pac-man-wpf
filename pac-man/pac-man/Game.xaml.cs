﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Media;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace pac_man
{
    public partial class Level : Window
    {
        private Hero heros { get; set; }
        private int music { get; set; }
        private SoundPlayer player = new SoundPlayer();
        private ImageBrush imgBrush = new ImageBrush();
        private int _directionX = 1;
        private int _directionY = 0;
        public List<Wall> wall = new List<Wall>();
        private int level { get; set; }
        public List<Dot> dots = new List<Dot>();
        public List<Ghost> ghosts = new List<Ghost>();
        public int FinalScore = 0;
        public string yourName = "NoName";
        public string YourName
        {
            get { return yourName; }
            set { yourName = value; }
        }
        Thread thread;
        private int pause;
        public SolidColorBrush scb;
        public string color;


        public Level(int level)
        {
            YourName = Read("name.txt");

            color = Read("brush.txt");
            if (color == "blue" || color.Length == 0) scb = Brushes.Blue;
            else if (color == "red") scb = Brushes.OrangeRed;
            else if (color == "green") scb = Brushes.LimeGreen;
            else if (color == "white") scb = Brushes.White;

            InitializeComponent();
            Closing += OnWindowClosing;
            pause = 0;

            ImageBrush imgBrush = new ImageBrush();
            imgBrush.ImageSource = new BitmapImage(new Uri(@"speaker.png", UriKind.Relative));

            if (Read("sound.txt").Length == 0) { music = 1; saveMusic();  MUSIC.Background = imgBrush; }
            else
            {
                music = int.Parse(Read("sound.txt"));
                if (music == 1)
                {
                    imgBrush.ImageSource = new BitmapImage(new Uri(@"speaker.png", UriKind.Relative));
                }
                else
                {
                    imgBrush.ImageSource = new BitmapImage(new Uri(@"mute.png", UriKind.Relative));
                }
                MUSIC.Background = imgBrush;
            }

            InitBoard();
            
            this.level = level;
            if (level == 1)
            {
                InitWallLevel1();
            }
            if (level == 2)
            {
                InitWallLevel2();
            }
            StartGame();
        }
        private void StartGame()
        {
            if (level == 1)
            {
                heros = new Hero(12, 11, grid);
                new Ghost(12, 9, @"ghost.png", ghosts, grid);
                new Ghost(11, 9, @"ghost2.png", ghosts, grid);
                new Ghost(11, 8, @"ghost3.png", ghosts, grid);
                myHero.DataContext = heros;
            }
            if (level == 2)
            {
                heros = new Hero(12, 11, grid);
                heros.Score = int.Parse(Read("score.txt"));
                heros.Lives = int.Parse(Read("lives.txt"));
                new Ghost(12, 9, @"ghost.png", ghosts, grid);
                new Ghost(11, 9, @"ghost2.png", ghosts, grid);
                new Ghost(11, 8, @"ghost3.png", ghosts, grid);
                new Ghost(12, 8, @"ghost4.png", ghosts, grid);
                myHero.DataContext = heros;
            }
            heros.DrawHero();
            foreach (Ghost ghost in ghosts)
            {
                ghost.Draw();
            }
            DrawWalls();
            DrawDots();

            thread = new Thread(goGhost);
            thread.Start();
        }
        private void DrawWalls()
        {
            foreach (Wall z in wall)
            {
                z.Draw();
            }
        }

        public void DrawDots()
        {
            foreach (Dot d in dots)
            {
                d.Draw();
            }
        }

        public void InitBoard()
        {
            for (int i = 0; i < grid.Width / 25; i++)
            {
                ColumnDefinition columnDefinitions = new ColumnDefinition();
                columnDefinitions.Width = new GridLength(25);
                grid.ColumnDefinitions.Add(columnDefinitions);
            }
            for (int j = 0; j < grid.Height / 25; j++)
            {
                RowDefinition rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(25);
                grid.RowDefinitions.Add(rowDefinition);
            }
        }

        public void Save(string filename, string item)
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\" + filename, FileMode.Create, FileAccess.Write);
            StreamWriter save = new StreamWriter(source);
            save.Write(item);
            save.Close();
            source.Close();
        }

        public void SaveNext(string filename, string item)
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\" + filename, FileMode.Append, FileAccess.Write);
            StreamWriter save = new StreamWriter(source);
            save.Write(item);
            save.Write(Environment.NewLine);
            save.Close();
            source.Close();
        }

        public void saveMusic()
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\sound.txt", FileMode.Create, FileAccess.Write);
            StreamWriter save = new StreamWriter(source);
            save.Write(music);
            save.Close();
            source.Close();
        }

        public string Read(string filename)
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\" + filename, FileMode.Open, FileAccess.Read);
            StreamReader read = new StreamReader(source);
            String line = read.ReadToEnd();
            read.Close();
            source.Close();
            return line;
        }

        private void ifYouWon()
        {
            if (dots.Count == 0)
            {
                level++;
                FinalScore = heros.Score;
                Save("score.txt", heros.Score.ToString());
                Save("lives.txt", heros.Lives.ToString());
                thread.Abort();
                if (level <= 2)
                {
                    new Level(level).Show();
                    this.Close();
                }
                else
                {
                    SaveNext("ranking.txt", heros.Score.ToString() + " " + YourName);
                    new Win().Show();
                    this.Close();
                }
            }
        }

        private void youLost()
        {
            Save("score.txt", heros.Score.ToString());
            Save("lives.txt", heros.Lives.ToString());
            SaveNext("ranking.txt", heros.Score.ToString() + " " + YourName);
            thread.Abort();
            new Lost().Show();
            this.Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                if (pause != 1)
                { 
                    heros.imgBrush.ImageSource = new BitmapImage(new Uri(@"man_l.png", UriKind.Relative));
                    _directionX = -1;
                    _directionY = 0;
                }
            }

            else if (e.Key == Key.Right)
            {
                if (pause != 1)
                {
                    heros.imgBrush.ImageSource = new BitmapImage(new Uri(@"man_p.png", UriKind.Relative));
                    _directionX = 1;
                    _directionY = 0;
                }
            }

            else if (e.Key == Key.Up)
            {
                if (pause != 1)
                {
                    heros.imgBrush.ImageSource = new BitmapImage(new Uri(@"man_g.png", UriKind.Relative));
                    _directionX = 0;
                    _directionY = -1;
                }
            }

            else if (e.Key == Key.Down)
            {
                if (pause != 1)
                {
                    heros.imgBrush.ImageSource = new BitmapImage(new Uri(@"man_d.png", UriKind.Relative));
                    _directionX = 0;
                    _directionY = 1;
                }
            }
            else if (e.Key == Key.P)
            {
                if (pause == 0)
                {
                    pause = 1;
                    thread.Abort();
                }
                else
                {
                    pause = 0;
                    thread = new Thread(goGhost);
                    thread.Start();
                }
                _directionX = 0;
                _directionY = 0;
            }
            dots = heros.MoveHero(_directionX, _directionY, dots, wall, Game);
            foreach (Ghost ghost in ghosts)
            {
                checkGhost(ghost);
            }

            if (heros.Lives < 0) youLost();
            ifYouWon();
        }

        public void goGhost()
        {
            while (true)
            {
                foreach (Ghost ghost in ghosts)
                {
                    checkGhost(ghost);

                    ghost.moveGhost(wall, Game);

                    checkGhost(ghost);

                    grid.Dispatcher.Invoke(() => { ghost.Draw(); });
                }
                Thread.Sleep(200);
            }
        }

        public void checkGhost(Ghost ghost)
        {
            if (heros.X == ghost.X && heros.Y == ghost.Y)
            {
                heros.Lives--;
                heros.X = 12;
                heros.Y = 11;
                grid.Dispatcher.Invoke(() => { heros.DrawHero(); });
                if (heros.Lives < 0)
                {
                    Application.Current.Dispatcher.Invoke(youLost);
                }
            }
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            thread.Abort();
        }

        public void InitWallLevel1()
        {
            //walls

            for (int i = 1; i < 16; i++)
            {
                new Wall(0, i, wall, grid, scb);
                new Wall(23, i, wall, grid, scb);
            }
            for (int j = 0; j < 24; j++)
            {
                new Wall(j, 1, wall, grid, scb);
                new Wall(j, 16, wall, grid, scb);
            }
            for (int i = 3; i < 8; i++)
            {
                new Wall(2, i, wall, grid, scb);
                new Wall(21, i, wall, grid, scb);
            }
            for (int i = 9; i < 15; i++)
            {
                new Wall(2, i, wall, grid, scb);
                new Wall(21, i, wall, grid, scb);
            }
            for (int j = 3; j < 7; j++)
            {
                new Wall(j, 5, wall, grid, scb);
                new Wall(j, 12, wall, grid, scb);
            }
            for (int j = 17; j < 21; j++)
            {
                new Wall(j, 5, wall, grid, scb);
                new Wall(j, 12, wall, grid, scb);
            }
            for (int j = 4; j < 7; j++)
            {
                new Wall(j, 3, wall, grid, scb);
                new Wall(j, 7, wall, grid, scb);
                new Wall(j, 9, wall, grid, scb);
                new Wall(j, 10, wall, grid, scb);
                new Wall(j, 14, wall, grid, scb);
            }
            for (int j = 17; j < 20; j++)
            {
                new Wall(j, 3, wall, grid, scb);
                new Wall(j, 7, wall, grid, scb);
                new Wall(j, 9, wall, grid, scb);
                new Wall(j, 10, wall, grid, scb);
                new Wall(j, 14, wall, grid, scb);
            }
            for (int j = 10; j < 14; j++)
            {
                new Wall(j, 12, wall, grid, scb);
            }
            for (int j = 10; j < 14; j++)
            {
                new Wall(j, 14, wall, grid, scb);
            }
            for (int j = 10; j < 14; j++)
            {
                new Wall(j, 3, wall, grid, scb);
            }
            for (int j = 10; j < 14; j++)
            {
                new Wall(j, 5, wall, grid, scb);
            }
            for (int i = 9; i < 15; i++)
            {
                new Wall(8, i, wall, grid, scb);
                new Wall(15, i, wall, grid, scb);
            }
            for (int i = 3; i < 8; i++)
            {
                new Wall(8, i, wall, grid, scb);
                new Wall(15, i, wall, grid, scb);
            }



            //for ghost
            for (int j = 10; j < 14; j++)
            {
                new Wall(j, 10, wall, grid, scb);
            }
            new Wall(10, 9, wall, grid, scb);
            new Wall(13, 9, wall, grid, scb);



            //dots

            for (int i = 2; i < 16; i++)
            {
                new Dot(1, i, dots, grid);
                new Dot(22, i, dots, grid);
            }
            for (int i = 3; i < 15; i++)
            {
                new Dot(7, i, dots, grid);
                new Dot(9, i, dots, grid);
            }
            for (int i = 3; i < 15; i++)
            {
                new Dot(14, i, dots, grid);
                new Dot(16, i, dots, grid);
            }
            for (int j = 2; j < 22; j++)
            {
                new Dot(j, 2, dots, grid);
                new Dot(j, 15, dots, grid);
            }
            for (int j = 3; j < 7; j++)
            {
                new Dot(j, 4, dots, grid);
                new Dot(j, 6, dots, grid);
                new Dot(j, 11, dots, grid);
                new Dot(j, 13, dots, grid);
            }
            for (int j = 10; j < 14; j++)
            {
                new Dot(j, 4, dots, grid);
                new Dot(j, 6, dots, grid);
                new Dot(j, 13, dots, grid);
            }
            for (int j = 17; j < 21; j++)
            {
                new Dot(j, 4, dots, grid);
                new Dot(j, 6, dots, grid);
                new Dot(j, 11, dots, grid);
                new Dot(j, 13, dots, grid);
            }
            new Dot(10, 11, dots, grid);
            new Dot(11, 11, dots, grid);
            new Dot(13, 11, dots, grid);
            for (int j = 2; j < 7; j++)
            {
                new Dot(j, 8, dots, grid);
            }
            for (int j = 17; j < 22; j++)
            {
                new Dot(j, 8, dots, grid);
            }
            new Dot(8, 8, dots, grid);
            new Dot(15, 8, dots, grid);
            new Dot(3, 3, dots, grid);
            new Dot(3, 7, dots, grid);
            new Dot(3, 9, dots, grid);
            new Dot(3, 10, dots, grid);
            new Dot(3, 14, dots, grid);
            new Dot(20, 3, dots, grid);
            new Dot(20, 7, dots, grid);
            new Dot(20, 9, dots, grid);
            new Dot(20, 10, dots, grid);
            new Dot(20, 14, dots, grid);
        }

        public void InitWallLevel2()
        {
            //walls

            for (int i = 1; i < 16; i++)
            {
                new Wall(0, i, wall, grid, scb);
                new Wall(23, i, wall, grid, scb);
            }
            for (int j = 0; j < 24; j++)
            {
                new Wall(j, 1, wall, grid, scb);
                new Wall(j, 16, wall, grid, scb);
            }
            for (int i = 3; i < 8; i++)
            {
                new Wall(2, i, wall, grid, scb);
                new Wall(21, i, wall, grid, scb);
            }
            for (int i = 9; i < 15; i++)
            {
                new Wall(2, i, wall, grid, scb);
                new Wall(21, i, wall, grid, scb);
            }
            for (int j = 4; j < 20; j++)
            {
                new Wall(j, 3, wall, grid, scb);
                new Wall(j, 14, wall, grid, scb);
            }
            for (int i = 5; i < 8; i++)
            {
                new Wall(4, i, wall, grid, scb);
                new Wall(19, i, wall, grid, scb);
            }
            for (int i = 9; i < 13; i++)
            {
                new Wall(4, i, wall, grid, scb);
                new Wall(19, i, wall, grid, scb);
            }
            for (int j = 6; j < 18; j++)
            {
                new Wall(j, 5, wall, grid, scb);
                new Wall(j, 12, wall, grid, scb);
            }
            for (int i = 7; i < 11; i++)
            {
                new Wall(6, i, wall, grid, scb);
                new Wall(17, i, wall, grid, scb);
            }
            new Wall(8, 7, wall, grid, scb);
            new Wall(15, 7, wall, grid, scb);
            new Wall(8, 10, wall, grid, scb);
            new Wall(15, 10, wall, grid, scb);

            //for ghost
            for (int j = 10; j < 14; j++)
            {
                new Wall(j, 10, wall, grid, scb);
            }
            new Wall(10, 9, wall, grid, scb);
            new Wall(13, 9, wall, grid, scb);



            //dots

            for (int i = 2; i < 16; i++)
            {
                new Dot(1, i, dots, grid);
                new Dot(22, i, dots, grid);
            }
            new Dot(2, 8, dots, grid);
            new Dot(4, 8, dots, grid);
            new Dot(19, 8, dots, grid);
            new Dot(21, 8, dots, grid);
            for (int j = 2; j < 22; j++)
            {
                new Dot(j, 2, dots, grid);
                new Dot(j, 15, dots, grid);
            }
            for (int j = 4; j < 20; j++)
            {
                new Dot(j, 4, dots, grid);
                new Dot(j, 13, dots, grid);
            }
            for (int i = 3; i < 15; i++)
            {
                new Dot(3, i, dots, grid);
                new Dot(20, i, dots, grid);
            }
            for (int i = 5; i < 13; i++)
            {
                new Dot(5, i, dots, grid);
                new Dot(18, i, dots, grid);
            }
            for (int i = 7; i < 11; i++)
            {
                new Dot(7, i, dots, grid);
                new Dot(16, i, dots, grid);
            }
            for (int j = 6; j < 12; j++)
            {
                new Dot(j, 6, dots, grid);
                new Dot(j, 11, dots, grid);
            }
            for (int j = 13; j < 18; j++)
            {
                new Dot(j, 6, dots, grid);
                new Dot(j, 11, dots, grid);
            }
            new Dot(12, 6, dots, grid);
        }

        private void MENU_Click(object sender, RoutedEventArgs e)
        {
            thread.Abort();
            new MainWindow().Show();
            this.Close();
        }

        private void MUSIC_Click(object sender, RoutedEventArgs e)
        {
            if(music==1)
            {
                imgBrush.ImageSource = new BitmapImage(new Uri(@"mute.png", UriKind.Relative));
                MUSIC.Background = imgBrush;
                SoundMute();
                music = 0;
                saveMusic();
            }
            else
            {
                imgBrush.ImageSource = new BitmapImage(new Uri(@"speaker.png", UriKind.Relative));
                MUSIC.Background = imgBrush;
                SoundMusic();
                music = 1;
                saveMusic();
            }
        }

        public void SoundMusic()
        {
            try
            {
                player.SoundLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\pacman.wav";
                player.PlayLooping();
            }
            catch { }
        }

        public void SoundMute()
        {
            try
            {
                player.Stop();
            }
            catch { }
        }
    }
}