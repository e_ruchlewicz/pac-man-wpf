﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace pac_man
{
    public class Ghost
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Rectangle image { get; set; }
        public ImageBrush imgBrush { get; set; }
        public Ghost()
        {

        }
        public Ghost(int x, int y, string uri, List<Ghost> ghosts, Grid grid)
        {
            this.X = x;
            this.Y = y;
            image = new Rectangle();
            image.Height = image.Width = 25;
            imgBrush = new ImageBrush();
            imgBrush.ImageSource = new BitmapImage(new Uri(uri, UriKind.Relative));
            image.Fill = imgBrush;
            grid.Children.Add(image);
            ghosts.Add(this);
        }
        public void Draw()
        {
            Grid.SetColumn(image, X);
            Grid.SetRow(image, Y);
        }
        public void moveGhost(List<Wall> wall, Window window)
        {
            int directionX = 0;
            int directionY = 0;

            int n = 0;
            while (n != 1)
            {
                Random rnd = new Random(Guid.NewGuid().GetHashCode());
                int r = rnd.Next()%4+1;
                if (r == 1)
                {
                    directionX = 1;
                }
                else if (r == 2)
                {
                    directionX = -1;
                }
                else if (r == 3)
                {
                    directionY = -1;
                }
                else if (r == 4)
                {
                    directionY = 1;
                }

                n = 1;

                foreach (Wall example in wall)
                {
                    if ((X + directionX) == example.X && (Y + directionY) == example.Y)
                    {
                        directionX = 0;
                        directionY = 0;
                        n = 0;
                    }
                }

            }
            X += directionX;
            Y += directionY;
        }
    }


}