﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace pac_man
{
    public class Wall : Ghost
    {
        public Wall(int x, int y, List<Wall> wall, Grid grid, SolidColorBrush wallBrush)
        {
            X = x;
            Y = y;
            image = new Rectangle();
            image.Width = 25;
            image.Height = 25;
            image.Fill = wallBrush;
            grid.Children.Add(image);
            wall.Add(this);
        }
    }
}