﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace pac_man
{
    public partial class Ranking : Window
    {
        public class Elem
        {
            public int score;
            public string name;

            public Elem(int score, string name)
            {
                this.score = score;
                this.name = name;
            }
        }

        private List<Elem> list = new List<Elem>();

        public string score1 = "0";
        public string score2 = "0";
        public string score3 = "0";
        public string score4 = "0";
        public string score5 = "0";

        public string Score1
        {
            get { return score1; }
            set { score1 = value; }
        }
        public string Score2
        {
            get { return score2; }
            set { score2 = value; }
        }
        public string Score3
        {
            get { return score3; }
            set { score3 = value; }
        }
        public string Score4
        {
            get { return score4; }
            set { score4 = value; }
        }
        public string Score5
        {
            get { return score5; }
            set { score5 = value; }
        }

        public Ranking()
        {
            Read("ranking.txt");
            List<Elem> SortedList = list.OrderByDescending(o => o.score).ToList();
            if(SortedList.Count > 0) Score1 = SortedList[0].score + " " + SortedList[0].name;
            if (SortedList.Count > 1) Score2 = SortedList[1].score + " " + SortedList[1].name;
            if (SortedList.Count > 2) Score3 = SortedList[2].score + " " + SortedList[2].name;
            if (SortedList.Count > 3) Score4 = SortedList[3].score + " " + SortedList[3].name;
            if (SortedList.Count > 4) Score5 = SortedList[4].score + " " + SortedList[4].name;
            DataContext = this;
            InitializeComponent();
        }

        public void Read(string filename)
        {
            var lines = File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\" + filename);
            foreach (var line in lines)
            {
                string[] splitString = line.Split();
                int score = int.Parse(splitString[0]);
                string name = splitString[1];
                list.Add(new Elem(score, name));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            this.Close();
        }
    }
}