﻿using System;
using System.IO;
using System.Windows;

namespace pac_man
{
    public partial class Lost : Window
    {
        public string score = "0";
        public string Score
        {
            get { return score; }
            set { score = value; }
        }

        public Lost()
        {
            Score = Read("score.txt");
            DataContext = this;
            InitializeComponent();
        }

        public string Read(string filename)
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\" + filename, FileMode.Open, FileAccess.Read);
            StreamReader read = new StreamReader(source);
            String line = read.ReadToEnd();
            read.Close();
            source.Close();
            return line;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            this.Close();
        }
    }
}
