﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace pac_man
{
    public class Hero : INotifyPropertyChanged
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Rectangle HeroImage;
        public ImageBrush imgBrush { get; set; }
        public int score;
        public int Score
        {
            get { return score; }
            set { score = value; OnPropertyChanged("Score"); }
        }
        public int lives;
        public int Lives
        {
            get { return lives; }
            set { lives = value; OnPropertyChanged("Lives"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Hero(int x, int y, Grid grid)
        {
            this.X = x;
            this.Y = y;
            this.Score = 0;
            this.Lives = 3;
            HeroImage = new Rectangle();
            HeroImage.Height = HeroImage.Width = 25;
            HeroImage.Height = HeroImage.Height = 25;
            imgBrush = new ImageBrush();
            imgBrush.ImageSource = new BitmapImage(new Uri(@"man_p.png", UriKind.Relative));
            HeroImage.Fill = imgBrush;
            grid.Children.Add(HeroImage);

        }
        public void DrawHero()
        {
            Grid.SetColumn(HeroImage, X);
            Grid.SetRow(HeroImage, Y);
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public List<Dot> MoveHero(int directionX, int directionY, List<Dot> dots, List<Wall> wall, Window window)
        {
            foreach (Wall example in wall)
            {
                if ((X + directionX) == example.X && (Y + directionY) == example.Y)
                {
                    directionX = 0;
                    directionY = 0;
                }
            }
            foreach (Dot dot in dots)
            {
                if ((X + directionX) == dot.X && (Y + directionY) == dot.Y)
                {
                    this.Score += 10;
                    dot.image.Fill = Brushes.Black;
                    dots.Remove(dot);
                    break;
                }
            }

            X += directionX;
            Y += directionY;
            DrawHero();

            return dots;
        }

    }
}