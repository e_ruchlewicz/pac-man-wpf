﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace pac_man
{
    public partial class Name : Window
    {
        public Name()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\name.txt", FileMode.Create, FileAccess.Write);
            StreamWriter save = new StreamWriter(source);
            if(name.Text.CompareTo(".......................")==0 || name.Text.Length==0) save.Write("NoName");
            else save.Write(name.Text);
            save.Close();
            source.Close();

            new Level(1).Show();
            this.Close();
        }

        private void CleanField(object sender, MouseEventArgs e)
        {
            name.Text = "";
        }
    }
}
