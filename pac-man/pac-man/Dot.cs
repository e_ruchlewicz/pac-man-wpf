﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace pac_man
{
    public class Dot : Ghost
    {

        public Dot(int x, int y, List<Dot> dots, Grid grid)
        {
            this.X = x;
            this.Y = y;
            image = new Rectangle();
            image.Height = image.Width = 25;
            ImageBrush imgBrush = new ImageBrush();
            imgBrush.ImageSource = new BitmapImage(new Uri(@"dot.png", UriKind.Relative));
            image.Fill = imgBrush;
            grid.Children.Add(image);
            dots.Add(this);
        }
    }
}