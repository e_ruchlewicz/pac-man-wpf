﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Media;
using System.ComponentModel;

namespace pac_man
{
    public partial class MainWindow : Window
    {
        private int music { get; set; }
        private SoundPlayer player = new SoundPlayer();
        private ImageBrush imgBrush = new ImageBrush();

        public MainWindow()
        {
            InitializeComponent();

            ImageBrush imgBrush = new ImageBrush();
            imgBrush.ImageSource = new BitmapImage(new Uri(@"speaker.png", UriKind.Relative));

            if (Read("sound.txt").Length == 0) { music = 1; saveMusic(); SoundMusic(); MUSIC.Background = imgBrush; }
            else
            {
                music = int.Parse(Read("sound.txt"));
                if (music == 1)
                {
                    imgBrush.ImageSource = new BitmapImage(new Uri(@"speaker.png", UriKind.Relative));
                }
                else
                {
                    imgBrush.ImageSource = new BitmapImage(new Uri(@"mute.png", UriKind.Relative));
                }
                MUSIC.Background = imgBrush;
            }
        }

        public void SoundMusic()
        { 
            try
            {
                player.SoundLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\pacman.wav";
                player.PlayLooping();
            }
            catch { }
        }

        public void SoundMute()
        {
            try
            {
                player.Stop();
            }
            catch { }
        }

        public string Read(string filename)
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\" + filename, FileMode.Open, FileAccess.Read);
            StreamReader read = new StreamReader(source);
            String line = read.ReadToEnd();
            read.Close();
            source.Close();
            return line;
        }

        private void Button_Gra(object sender, RoutedEventArgs e)
        {
            new Name().Show();
            this.Close();
        }

        private void Button_Ranking(object sender, RoutedEventArgs e)
        {
            new Ranking().Show();
            this.Close();
        }

        private void Button_Zamknij(object sender, RoutedEventArgs e)
        {
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\brush.txt", string.Empty);
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\sound.txt", string.Empty);
            this.Close();
        }

        private void MUSIC_Click(object sender, RoutedEventArgs e)
        {
            if (music == 1)
            {
                imgBrush.ImageSource = new BitmapImage(new Uri(@"mute.png", UriKind.Relative));
                MUSIC.Background = imgBrush;
                SoundMute();
                music = 0;
                saveMusic();
            }
            else
            {
                imgBrush.ImageSource = new BitmapImage(new Uri(@"speaker.png", UriKind.Relative));
                MUSIC.Background = imgBrush;
                SoundMusic();
                music = 1;
                saveMusic();
            }
        }

        public void saveMusic()
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\sound.txt", FileMode.Create, FileAccess.Write);
            StreamWriter save = new StreamWriter(source);
            save.Write(music);
            save.Close();
            source.Close();
        }

        public void saveColor(String color)
        {
            FileStream source = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Pacman\brush.txt", FileMode.Create, FileAccess.Write);
            StreamWriter save = new StreamWriter(source);
            save.Write(color);
            save.Close();
            source.Close();
        }

        private void blue_Click(object sender, RoutedEventArgs e)
        {
            saveColor("blue");
        }

        private void red_Click(object sender, RoutedEventArgs e)
        {
            saveColor("red");
        }

        private void green_Click(object sender, RoutedEventArgs e)
        {
            saveColor("green");
        }

        private void white_Click(object sender, RoutedEventArgs e)
        {
            saveColor("white");
        }
    }
}
